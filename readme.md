# passwdqc notes

## For Centos 6 Systems

 1. yum update
 2. yum install pam_passwdqc
 3. Add USEPASSWDQC=yes to /etc/sysconfig/authconfig
 4. Add the below passwdqc policy to /etc/pam.d/system-auth
 5. Run authconfig --updateall as root

 ```bash
     password        requisite                       pam_passwdqc.so      min=20,20,12,12,12 max=64 passphrase=4 similar=deny enforce=everyone
 ```
  
## For Debian/Ubuntu Systems
  1. aptitude install libpam-passwdqc
  2. pam-auth-update
  3. Edit the pam_passwdqc.so line in /etc/pam.d/common-password to look like this to allow Stanford model:

  ```bash
      password        requisite                       pam_passwdqc.so      min=20,20,12,12,12 max=64 passphrase=4 similar=deny enforce=everyone
  ```

## New VT Password Rules (2018 and beyond)
       
```bash
   min=20,20,12,12,12
   max=64
   passphrase=5
   similar=deny
   enforce=everyone
```

## Old VT Password Rules (do not use)

```bash
  min=disabled,disabled,disabled,disabled,8
  max=64
  passphrase=0
  similar=deny 
  enforce=everyone
```

## Misc

[passwdqc documentation](https://www.openwall.com/passwdqc/README)


